import React from 'react'
// this module was created to provide the necessary info about the user or client who is using the app

// 1. Create the context about the topic.
	// createContext() will allow us to create a context or object that will allow us store information about the subject.
const UserContext = React.createContext(); //container

// 2. acquire for the provider property of the newly created context object
	// visualize
	// UserContext = {
	// propsAboutTheUser: value
		// Provider:
	// }
	// Provider => this will allow us to use its utility to permit subcomponents of our app to consume/use the available information about our subject. you need to expose this utility/data
export const UserProvider = UserContext.Provider;

// 3. Identify the module on your app that will take the mantle/role of the provider
export default UserContext;