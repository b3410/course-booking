
//identify the components needed to create the register page

import {useState, useEffect, useContext} from 'react'
import Hero from './../components/Banner';
import {Container, Form, Button} from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
const data = {
	title: 'Welcome to the Register Page',
	content: 'Create an Account to enroll'
};
//declare a statefor input fields in theregister page
//create a 'side effet if '
export default function Register () {
	//destructure the data from the context object for the user
	const {user} = useContext(UserContext);
	
	// we will decalre a state for the following components to make sure that they will be empty from the start
	//once we passed down the variables into their respectuve components they will bounded to its value 
	//for us to manage state of our components we will need a 'Hook' in react that will aloow us to handle state changes within the component
	//using the acquired hook from react, we will now apply a 2 way binding for the input elements
	//syntax: [getter, setter] = useState()
	//set + variable
	//Contain variables into one dimensional structure
	//we will use an eventEmitter that will allow us to change the states
	//ATM we applied/created a 1 way binding in our component, this 2 way binding will allow us to create/manage information all across our components
	//check if you successfullt caught the data in each component
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	//decalre a state for the button component, attack a state to eventually manage and change the state of the button
	//to evetually change the state of the button, insert a state setter
	//now using the state hook, apply a conditional rendering to our button component
	//naming convention for a boolean values?
		//is (prefix) + verb 
		// => YES or NO/ TRUE or FALSE

	const [isActive, setIsActive] = useState(false);
	//make a reactive response method using a conditional rendering
	const [isMatched, setIsMatched] = useState(false);
	const [isMobileValid, setIsMobileValid] = useState(false);
	const [isAllowed, setIsAllowed] = useState(false);
	//describe the effects that will react on how the user will interact with the from component 
	//to remove the warnings we will provide the dependencies list
	useEffect(() => {
		//make the reactions swift
		//we will modify the control structure to best suit our set rules bound by the register page (long version)
		if (mobileNo.length === 11) 	
		{
			//this block of code will run if the 1st rule above was met
			setIsMobileValid(true);
			if (password1 === password2 && password1 !== '' && password2 !== '') {
				setIsMatched(true);
				if (firstName !=='' && lastName !=='' && email !=='') {
					setIsAllowed(true);
					setIsActive(true);
				} else {
					setIsAllowed(false);
					setIsActive(false);
				}
				
			} else {
				setIsMatched(false);
				setIsActive(false);
				setIsAllowed(false);
			}
		
		} 
		else if(password1 !== '' && password1 === password2) {
			setIsMatched(true);
		}
		else {
			setIsActive(false);
			setIsMatched(false);
			setIsMobileValid(false);
			setIsAllowed(false);
		};

	}, [firstName, lastName, email, password1, password2, mobileNo]);
	//the effect 'hook' 'useEffect()' will use the dependencies list in order to identify which components to watch out for changes to avoid continuos form submission for every render of each component
	const registerUser = async (eventSubmit) => {
		eventSubmit.preventDefault()
		/*console.log(firstName);
		console.log(lastName);
		console.log(email);
		console.log(mobileNo);
		console.log(password1);
		console.log(password2);*/

		// we will now modify this funcction so that we will be able to send a request to our API so that the client will be able to create his own account
		//syntax: fetch('url', {options})
		//keep in mind upon sending the data to the api
		//upon sending this request a promise will be initialized 
		//include an await expression in the fetch to make sure that react will finish the task first before proceeding to the next
		const isRegistered = await fetch(`https://warm-forest-76823.herokuapp.com/users/register/`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password1,
				mobileNo: mobileNo
			})
		}).then(res => res.json()).then(data => {
			console.log(data)
			//ceate a control structure if success or fail
			//check if the email props has data
			if (data.email) {
				return true;
			} else {
				//failure
				return false;
			}
		})
		//create a control structure that will evaluate the result of the fetch method
		if (isRegistered) {
			await Swal.fire(
					{
						icon:"success",
						title:"Registration Successful!",
						text: "Thank you for creating an account!"

					}
				)
			setFirstName('');
			setLastName('');
			setEmail('');
			setMobileNo('');
			setPassword1('');
			setPassword2('');				 
			//display a message that will confirm to the user that registration is successful
			window.location.href = '/login'
		} else {
			//response if registration has failed
			await Swal.fire(
					{
						icon:"error",
						title:"Registration Error!",
						text: "Try Again Later!"

					}
				)
		}

			
	};

	return(
		user.id
		?
			<Navigate to="/courses" replace={true}/>
		:
		<>
			<Hero bannerData={data}/>
			<Container>
				{
					isAllowed ? 
					<h1 className="text-center text-success">You May Now Register!</h1>
					:
					<h1 className="text-center">Register Form</h1>
				}
				<h6 className="mt-3 text-center text-secondary">Fill up the form below</h6>
				<Form onSubmit={e => registerUser(e)}>
					{/*First Name Field*/}
					<Form.Group>
						<Form.Label>First Name: </Form.Label>
						<Form.Control type="text" placeholder="Enter Your First Name" required value={firstName} onChange={event => {setFirstName(event.target.value)} } />
					</Form.Group>

					{/*Last Name Field*/}
					<Form.Group>
						<Form.Label>Last Name: </Form.Label>
						<Form.Control type="text" placeholder="Enter Your Last Name" required value={lastName} onChange={event => {setLastName(event.target.value)} } />
					</Form.Group>

					{/*Email*/}
					<Form.Group>
						<Form.Label>Email: </Form.Label>
						<Form.Control type="email" placeholder="Enter Your Email" required value={email} onChange={event => {setEmail(event.target.value)} } />
					</Form.Group>

					{/*Mobile*/}
					<Form.Group>
						<Form.Label>Mobile Number: </Form.Label>
						<Form.Control type="number" placeholder="Enter Your Mobile Number" required value={mobileNo} onChange={event => {setMobileNo(event.target.value)} }/>
						{
							isMobileValid ?
						<span className="text-success">Mobile No. is now Valid!</span>
						:
						<span className="text-muted">Mobile No. Should be 11-digits</span>
						}
					</Form.Group>

					{/*Password*/}
					<Form.Group>
						<Form.Label>Password: </Form.Label>
						<Form.Control type="password" placeholder="Enter Your Password" required value={password1} onChange={event => {setPassword1(event.target.value)} }/>
					</Form.Group>

					{/*Reconfirm Password*/}
					<Form.Group>
						<Form.Label>Reconfirm Password: </Form.Label>
						<Form.Control type="password" placeholder="Confirm Your Password" required value={password2} onChange={event => {setPassword2(event.target.value)} }/>
							{
								isMatched ? 
							<span className="text-success">Passwords Match!</span>
								:
							<span className="text-danger">Passwords Should Match!</span>	
							}
					</Form.Group>

					{/*Register Button*/}
					{
						isActive ? 

						<Button variant="success" className="btn-block" type="submit">Register
						</Button>
							:
						<Button variant="success" className="btn-block" disabled>Register
						</Button>
					}

				</Form>
			</Container>
		</>
		);

}