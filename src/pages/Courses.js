//Identify which components will be displayed 
import {Container, Row, Col} from 'react-bootstrap'
import Hero from './../components/Banner';
import CourseCard from './../components/CourseCard';
import { useState, useEffect } from 'react';
const bannerDetails = {
	title: 'Course Catalog',
	content: 'Browse through our Catalog of Courses'
}
//Catalog all active courses
	//1. Container for each course that we will retrieve from the database
	//2. declare a state for the courses collection is an empty array.
	//3. Create a side effect what will send a request to our backend api project
	//4. Pass down the retrieved resourcces inside the compoenents as props
export default function Courses () {
	//this array/storage will be used to save ourr course components for all active courses.
	const [coursesCollection, setCourseCollection] = useState([]);
	//create an effect using our effect Hook, provide a lisy of dependencies
	//the effect that we are going to create is fetching data from the server
	useEffect(() => {
		//fetch() => is a JS method that will allow us to pass/create a request to an API
		//SYNTAX: fetch(<request URL>, {OPTIONS}) 
		//GET HTTP METHOD especially since we don't need to request an access token => NO NEED TO INSERT OPTIONS
		//remember that once you send a request to an endpoint, a 'promise' is returned as a result. (fulfilled, rejected, pending)
		//we need to be able to handle the outcome of the promise
		//we need to make the response usable on the frontend side. we need to convert it to a json format
		//upon converting the fetch data to a json format a new promise will be executed
		fetch('https://warm-forest-76823.herokuapp.com/courses/').then(res => res.json()).then(convertedData => {
			/*console.log(convertedData);*/
			//we want the resources to be displyed in the page
			//since the data are stored in an array storage, we will iterate the array to explivity get each document one by one.
			//there will multiple course card omponenr that will be rendered that corresponds to each document retrieved from the database. we need to utilize a storage to contain the cards that will be rendered
			//key attribute => as a reference to avoid rendering duplicates of the same element
			setCourseCollection(convertedData.map(course => {
				return(
					<CourseCard key={course._id} courseProp={course} />
					)
			})) 
		})
	}, [])
	return (
		<>
			<Hero bannerData={bannerDetails}/>
			<Container>
				<Row className="e-card e-card-horizontal">
					<Col>
						{coursesCollection}
					</Col>
				</Row>
			</Container>
		</>
		);
}