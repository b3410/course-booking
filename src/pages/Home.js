//acquire all the components that will make up the home page. (hero section, highlights)

import Banner from './../components/Banner'
import Highlights from './../components/Highlights'
//Identify first how you want the landing page to be structured.
//when wrappimg multiple adjacent JSX Elements
	//=> div element
	//=> React Fragment - group elements
						// - invisible div
						//-Container
			//syntax: long <React.Fragment> || <Fragment>
					//short <> </>

//lets create a data object that will describe the content of the hero section

const data = {
	title: 'Welcome to the Home Page',
	content: 'Opportunities for everyone, everywhere'
};
export default function Home() {
	return(
		<div>
			<Banner bannerData={data}/>
			<Highlights />
		</div>
		);
};