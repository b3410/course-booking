//This will serve as page whenever their lient would want to select a single course/item from the catalog.
import {useState, useEffect} from 'react'
import Banner from './../components/Banner';
//Grid System, Card, Button
import {Row, Col, Card, Button, Container} from 'react-bootstrap';

//declare a state for the course dateils. we need a correct 'hook'


//routing component
import {Link, useParams} from 'react-router-dom';

//import sweetalert
import Swal from 'sweetalert2';

const data = {
	title:'Welcome to B156 Booking-App',
	content:'Check out our school campus'
}

export default function CourseView() {
	//state our course details
	const [courseInfo, setCourseInfo] = useState({
			name: null,
			description: null,
			price: null
		});

	//retieve the data from the url to extract the course id that will be used to determine which course will be displayed in the page. Acquire a hook that will allow us to manage/access the data in the browser url
	//using the parameter Hook
	//(useState, useEffect, useParams) 
	//the useParams will return an object that will contain the path variables stored in the URL
	//take a peek at the data insise the URL
	console.log(useParams())
	//observe, it should contain an obejct that stores the id of the targeted course
	//extarct the value from the path variables by destructutig the object

	const {id} = useParams()
	console.log(id)
	//Create a "side effect" which will send a reuest to our backend API for course-booking. Use the proper 'hook' (effect hook)
	useEffect(() => {
		//send a request to our API to retrieve information about the course
		//SYNTAX: fetch('<URL>', '<OPTIONS>')
		//uppon sending this request to the API a promise will be initilized
		fetch(`https://warm-forest-76823.herokuapp.com/courses/${id}`).then(res => res.json()).then(convertedData => {
			console.log(convertedData)
			//the data that we retrieved from the db, we need to contain
			//call the setter to change the state of the course info for this page
			setCourseInfo({
				name: convertedData.name,
				description: convertedData.description,
				price: convertedData.price
			})
		})
	},[id])

	const enroll = () => {
		return (
				Swal.fire({
					icon: 'success',
					title:'Enrolled Sucessfully!',
					text:'Thank you for enrolling to this course'
				})
			)
	};

	return(
		<>
			<Banner bannerData={data} />
			<Row>
				<Col>
					<Container>
						<Card className="text-center">
							<Card.Body>
								{/*Course Name*/}
								<Card.Title>
									<h3>{courseInfo.name}</h3>
								</Card.Title>
								{/*Course Desscription*/}
								<Card.Subtitle>
									<h6 className="my-4">Description: </h6>
								</Card.Subtitle>
								<Card.Text>
									{courseInfo.description}
								</Card.Text>
								{/*Course Price*/}
								<Card.Subtitle className="my-4">
									<h6>Price: </h6>
								</Card.Subtitle>
								<Card.Text>
									{courseInfo.price}
								</Card.Text>
							</Card.Body>

							<Button variant="warning" className="btn-block" onClick={enroll}>
								Enroll
							</Button>
							<Link className="btn btn-success btn-block mb-5" to="/login">
								Login to Enroll
							</Link>
						</Card>
					</Container>
				</Col>
			</Row>
		</>
			);
}