//Container for each course

//Identify needed components
import {Card} from 'react-bootstrap';
import {Link} from 'react-router-dom'
//we are going to use this link component to not only redirect the user the course page but uo carry data needed to identify which course
export default function CourseCard({courseProp}) {
	return(
		
	
				
				<Card className="course-card m-4 d-md-inline-flex d-sm-inline-flex">
					<Card.Body>
						<Card.Title>
							{courseProp.name}
						</Card.Title>
						<Card.Text>
							{courseProp.description}
						</Card.Text>
						<Card.Text>
							Price: {courseProp.price}
						</Card.Text>
						<Link to={`view/${courseProp._id}`} className="btn btn-primary">
							View Course
						</Link>
					</Card.Body>
				</Card>
						
			
	
		
		);
}